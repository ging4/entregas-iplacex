package com.example.prueba1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class VistaEmpleadoRegular : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empleado_regular)

        val btnVolver = findViewById<Button>(R.id.btnVolver)
        btnVolver.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        val etBruto = findViewById<EditText>(R.id.etBruto)
        val tvResultado = findViewById<TextView>(R.id.tvResultado)

        val btnCalcular = findViewById<Button>(R.id.btnCalcular)
        btnCalcular.setOnClickListener {
            val empleado = EmpleadoRegular(etBruto.text.toString().toDouble())
            val sueldo = empleado.calcularLiquido()
            tvResultado.text = "Su sueldo es ${sueldo}"
        }
    }
}