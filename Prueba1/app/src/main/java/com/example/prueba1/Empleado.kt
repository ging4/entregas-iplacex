package com.example.prueba1

abstract class Empleado() {
    protected abstract val sueldoBruto:Double
    abstract fun calcularLiquido():Double
}

class EmpleadoHonorarios(override val sueldoBruto: Double) : Empleado() {
    override fun calcularLiquido(): Double {
        return (sueldoBruto - ((sueldoBruto * 13) / 100))
    }
}
class EmpleadoRegular(override val sueldoBruto: Double) : Empleado() {
    override fun calcularLiquido(): Double {
        return (sueldoBruto - ((sueldoBruto * 20) / 100))
    }
}