package com.example.prueba1

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

class VistaEmpleadoHonorarios : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PantallaHonorarios()
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview(showBackground = true)
@Composable
fun PantallaHonorarios(){
    val contexto = LocalContext.current

    var sueldoBruto by remember {mutableStateOf("")}
    var resultado by remember {mutableStateOf("")}

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Cyan),
    ) {
        Button(onClick = {
            val intent = Intent(contexto, MainActivity::class.java)
            contexto.startActivity(intent)
        }) {
            Text(text = "<- Volver")
        }
        Spacer(modifier = Modifier.height(20.dp))
        Text("""Calculo sueldo liquido: 
            |Empleados a Honorarios""".trimMargin())
        Spacer(modifier = Modifier.height(20.dp))
        TextField(
            value = sueldoBruto,
            onValueChange = {sueldoBruto = it},
            label = {Text("Sueldo Bruto")},
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )
        Spacer(modifier = Modifier.height(20.dp))
        Text(resultado)
        Button(onClick = {
            // valida el input en sueldoBruto, si es null asigna un cero
            val sueldoBrutoValidacion = sueldoBruto.toDoubleOrNull() ?: 0
            // si validacion retorna distinto a cero continuar con el calculo
            if (sueldoBrutoValidacion != 0 ){
                /* Validacion devuelve un dato tipo Number, sin embargo mi constructor
                 necesita un Double, asique una vez confirmada la validez del input,
                 se convierte el valor a double y la aplicacion funciona correctamente */
                val sueldoBrutoDouble = sueldoBrutoValidacion.toDouble()
                val empleado = EmpleadoHonorarios(sueldoBrutoDouble)
                val sueldoLiquido = empleado.calcularLiquido()
                resultado = "Su sueldo es: " + sueldoLiquido
            } else {
                resultado = "Su sueldo es invalido."
            }
        }) {
            Text(text = "Calcular")
        }
    }
}